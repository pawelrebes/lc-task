# LiveChat recruitment task

## Setup
Install npm dependencies `npm install`  
Run Cypress `npm run cypress:open`  
For headless mode run `npm run test`  

## Known issues
This was my first attempt to integrate Cypress with Cucumber, for which I used [cypress-cucumber-preprocessor](https://github.com/TheBrainFamily/cypress-cucumber-preprocessor). All stories are stored in .feature files and stored in features folder. Steps are defined in common.js file. Unfortunately, some unexpected issues occured:
### Session not cleared between features
For some reason the browser cache is stored between stories, which is against the general Cypress rules. As a workaround, the "Access platform feature" must be run before the rest of test suite. On consecutive runs ths test will fail unless the browser storage is purged manually.
### All step definitions stored in common.js
Steps scoped by feature should be stored in separate .js files and only reusable steps should be kept in common.js. However, cypress couldn't find the step definitions outside the common.js file.
### Arbitrary use of wait()
The wait() function should be avoided as it may cause test flakiness. Instead of waiting for a defined period of time, the test should wait for a request before continuing with tests.
### Low code reusability
Some step definitions should be more reusable, instead of pointing out a specific string in step definition, the `{string}` attribute should be used. This however did not work, so a workaround was applied.