// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
const accountsURL = 'https://accounts.labs.livechat.com';
const accountsParams =
  '?client_id=58737b5829e65621a45d598aa6f2ed8e&response_type=token&redirect_uri=https%3A%2F%2Fmy.labs.livechatinc.com';
const agentEmail = 'pawel@rebes.pl';
const agentPassword = 'passpass';
function getParamsFromHashUrl(url) {
  return url
    .split('#')
    .pop()
    .split('&')
    .reduce(function (acc, curr) {
      const [key, value] = curr.split('=');
      acc[key] = decodeURIComponent(value);
      return acc;
    }, {});
}
const loginRequest = function (email, password) {
  return cy.request({
    method: 'POST',
    url: `${accountsURL}${accountsParams}`,
    headers: {
      Origin: accountsURL,
      Referer: accountsURL,
    },
    form: true,
    followRedirect: false,
    body: {
      email,
      password,
    },
  });
};
Cypress.Commands.add('login', function (
  email = agentEmail,
  password = agentPassword
) {
  return loginRequest(email, password).then(function (response) {
    const params = getParamsFromHashUrl(response.headers.location);
    const agent = { email, value: { accessToken: params.access_token } };
    return agent;
  });
});