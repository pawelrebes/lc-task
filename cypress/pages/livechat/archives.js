const LOGIN_FIELD = 'input[type="email"]';
const PASSWORD_FIELD = 'input[type="password"]';
const SEARCH_BAR = 'input[placeholder="Search in archives…"]';
const CHATS_LIST = 'div[data-testid="list-container"]';
const ADD_FILTER_BUTTON = 'button[data-testid="add-filter-button"]';
const FILTER_DROPDOWN = 'ul[data-testid="dropdown-list"]';
const DATE_RANGE_FILTER = 'div[id="date-range-filter"]';
const DATE_SELECTED_FILTER = 'div[data-testid="date-filter"]';
const DATE_RANGE_INPUT = 'input[placeholder="YYYY-MM-DD"][tabindex="0"]';
const AGENT_FILTER = 'div[id="agent-filter"]';
const RATING_FILTER = 'div[id="rating-filter"]';
const TAG_FILTER = 'div[id="tag-filter"]';

class archives {
    static visit() {
        cy.visit('https://my.labs.livechatinc.com/archives/');
        cy.wait(5000);
    }

    static manualLogin() {
        cy.get(LOGIN_FIELD).type('m.debski+frontend_tests@livechatinc.com');
        cy.get(PASSWORD_FIELD).type('test1@3$');
        cy.get('button').contains('Sign in').click();
        cy.wait(5000);
    }

    static focusOnSearchBar() {
        cy.get(SEARCH_BAR).click();
    }

    static typeInSearchBar(query) {
        cy.get(SEARCH_BAR).type(query);
    }

    static assertInChatsList(query) {
        cy.get(CHATS_LIST).contains(query);
    }

    static clickAddFilterButton() {
        cy.get(ADD_FILTER_BUTTON).click();
    }

    static pickFilter(query) {
        cy.get(FILTER_DROPDOWN).contains(query).click();
    }

    static pickFilterDate(query) {
        cy.get(DATE_RANGE_FILTER).contains(query).click();
    }

    static assertFilterDate(query) {
        cy.get(DATE_SELECTED_FILTER).contains(query);
    }

    static noResults() {
        cy.get(CHATS_LIST).contains("Darn, no results found");
    }

    static customDatePeriod() {
        cy.get(DATE_RANGE_INPUT).click().type("2019-11-30").tab().type("2019-11-31").wait(100);
    }

    static selectAgent(query) {
        cy.get(AGENT_FILTER).contains(query).click().wait(200);
    }

    static selectRating(query) {
        cy.get(RATING_FILTER).contains(query).click().wait(200);
    }

    static selectTag(query) {
        cy.get(TAG_FILTER).contains(query).click().wait(200);
    }
}

export default archives;