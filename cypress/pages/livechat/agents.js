const AGENTS_MODULE = 'div[id=agents]';
const AGENTS_TAB = 'a[href="/agents"]';
const CHATBOTS_TAB = 'a[href="/agents/chatbots"]';
const GROUPS_TAB = 'a[href="/agents/groups"]';
const DESKTOP_ICON = 'span[title="Desktop"]';
const AGENT_DETAILS_BUTTON = 'svg[data-testid="agent-details-button"]';
const GROUP_DETAILS = 'span[class="group-name"]';
const ADDITIONAL_INFO = 'dl[class="additional-info"]';

class agents {
    static visitAgents() {
        cy.visit('https://my.labs.livechatinc.com/agents');
        cy.wait(5000);
    }

    static assertTeamSectionName() {
        cy.get(AGENTS_MODULE).contains('Team');
    }

    static assertTabs() {
        cy.get(AGENTS_MODULE).find(AGENTS_TAB);
        cy.get(AGENTS_MODULE).find(CHATBOTS_TAB);
        cy.get(AGENTS_MODULE).find(GROUPS_TAB);
    }

    static assertDesktopIcon() {
        cy.get(AGENTS_MODULE).find(DESKTOP_ICON);
    }

    static loggedInAgents() {
        cy.get(AGENTS_MODULE).contains('pawel@rebes.pl');
    }

    static selectAgent() {
        cy.get(AGENTS_MODULE).contains('pawel@rebes.pl').click();
        cy.get(AGENT_DETAILS_BUTTON).click();
    }

    static groupDetails() {
        cy.get(AGENTS_MODULE).find(GROUP_DETAILS).contains('General');
        cy.get(AGENTS_MODULE).find(ADDITIONAL_INFO).contains('Status after login:');
        cy.get(AGENTS_MODULE).find(ADDITIONAL_INFO).contains('Chats limit:');
        cy.get(AGENTS_MODULE).find(ADDITIONAL_INFO).contains('Daily summary:');
    }
}

export default agents;