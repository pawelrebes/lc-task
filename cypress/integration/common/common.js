import { Given, When, And, Then } from 'cypress-cucumber-preprocessor/steps';
import agents from '../../pages/livechat/agents';
import archives from '../../pages/livechat/archives';

beforeEach(() => {
    sessionStorage.clear()
    cy.clearCookies()
    cy.clearLocalStorage()
});

Given(/^I’m on the Livechat Archives page$/, () => {
    archives.visit();
});

And(/^I’m logged in as a LiveChat Business User$/, () => {
    archives.customLogin();
});

// When(/^I type {string} in Archives Search Bar$/, (query) => {
//     archives.typeInSearchBar(query);
// });


// Then(/^I should see chat with {string} in chats list$/, (query) => {
//     archives.assertInChatsList(query);
// });

When (/^I type "Client no2" in Archives Search Bar$/, () => {
    archives.typeInSearchBar("Client no2");
});

Then(/^I should see chat with "Client no2" in chats list$/, () => {
    archives.assertInChatsList("Client no2");
});

When (/^I type "Agent 007" in Archives Search Bar$/, () => {
    archives.typeInSearchBar("Agent 007");
});

Then(/^I should see chat with "Agent 007" in chats list$/, () => {
    archives.assertInChatsList("Agent 007");
});

When (/^I type "Facebook" in Archives Search Bar$/, () => {
    archives.typeInSearchBar("Facebook");
});

Then(/^I should see chat with "Facebook" in chats list$/, () => {
    archives.assertInChatsList("Facebook");
});

And(/^today’s date is 30 Nov 2019$/, () => {
    archives.setDateToNovember();
});

When (/^I click on Add filter button$/, () => {
    archives.clickAddFilterButton();
});

And (/^I pick Date option$/, () => {
    archives.pickFilter("Date");
});

And (/^I select Today option$/, () => {
    archives.pickFilterDate("Today");
});

Then (/^I should see Today date filter is selected$/, () => {
    archives.assertFilterDate("Today");
});

And (/^I should see "Darn, no results found" in chats list$/, () => {
    archives.noResults();
});

And (/^I select Custom period$/, () => {
    archives.pickFilterDate("Custom");
});

And (/^I provide dates from 2019-11-30 to 2019-11-31$/, () => {
    archives.customDatePeriod();
});

Then(/^I should see chat with "vv" in chats list$/, () => {
    archives.assertInChatsList("vv");
});

And (/^I pick Agent option$/, () => {
    archives.pickFilter("Agent");
});

And (/^I select "Agent 007"$/, () => {
    archives.selectAgent("Agent 007");
});

And (/^I pick Rating option$/, () => {
    archives.pickFilter("Rating");
});

And (/^I select "Any rating"$/, () => {
    archives.selectRating("Any rating");
});

Then(/^I should see chat with "AgentTestowy" in chats list$/, () => {
    archives.assertInChatsList("AgentTestowy");
});

And (/^I pick Tag option$/, () => {
    archives.pickFilter("Tag");
});

And (/^I select "positive feedback"$/, () => {
    archives.selectTag("positive feedback");
});

Given (/^I am logged in as a Trial User$/, () => {
    cy.login();
});

And (/^I am on the Agents page$/, () => {
    agents.visitAgents();
});

Then (/^I should see the "Team" section name$/, () => {
    agents.assertTeamSectionName();
});

And (/^I should see tabs: "Agents", "Chatbots", "Groups"$/, () => {
    agents.assertTabs();
});

And (/^I should see the "Desktop" icon$/, () => {
    agents.assertDesktopIcon();
});

And (/^I should see the logged Agent$/, () => {
    agents.loggedInAgents();
});

When (/^I select an Agent$/, () => {
    agents.selectAgent();
});

Then (/^I should see Agent's details: "Groups", "Additional info"$/, () => {
    agents.groupDetails();
});