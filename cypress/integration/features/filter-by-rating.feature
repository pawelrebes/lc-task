Feature: Filter by Rating
    As a LiveChat Business User
    In order to find conversations with a particular rating
    I want to use the Rating filter

    @e2e-test

    Scenario: Filter by Rating
        Given I’m on the Livechat Archives page
        When I click on Add filter button
        And I pick Rating option
        And I select "Any rating"
        Then I should see chat with "AgentTestowy" in chats list