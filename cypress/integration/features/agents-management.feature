Feature: Validate Agents view
    As a LiveChat Business User
    In order to use the Agents section
    I want to see if data is correct

    @e2e-test

    Scenario: Agents view
        Given I am logged in as a Trial User
        And I am on the Agents page
        Then I should see the "Team" section name
        And I should see tabs: "Agents", "Chatbots", "Groups"
        And I should see the "Desktop" icon
        And I should see the logged Agent
        When I select an Agent
        Then I should see Agent's details: "Groups", "Additional info"