Feature: Filter by Date
    As a LiveChat Business User
    In order to find conversations from a particular period
    I want to use the date filter

    @e2e-test

    Scenario: Filter by custom period
        Given I’m on the Livechat Archives page
        When I click on Add filter button
        And I pick Date option
        And I select Custom period
        And I provide dates from 2019-11-30 to 2019-11-31
        Then I should see chat with "vv" in chats list

    Scenario: Show today’s conversations
        Given I’m on the Livechat Archives page
        When I click on Add filter button
        And I pick Date option
        And I select Today option
        Then I should see Today date filter is selected
        And I should see "Darn, no results found" in chats list