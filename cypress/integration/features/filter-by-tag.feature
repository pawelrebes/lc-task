Feature: Filter by Tag
    As a LiveChat Business User
    In order to find conversations with a particular tag
    I want to use the Tag filter

    @e2e-test

    Scenario: Filter by Tag
        Given I’m on the Livechat Archives page
        When I click on Add filter button
        And I pick Tag option
        And I select "positive feedback"
        Then I should see chat with "AgentTestowy" in chats list
