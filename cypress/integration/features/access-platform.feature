Feature: Access the platform
    As a LiveChat Business User
    In order to access the LiveChat platform
    I want to log in

    @e2e-test

    Scenario: Log in
        Given I’m on the Livechat Archives page
        And I’m logged in as a LiveChat Business User