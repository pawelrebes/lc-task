Feature: Filter by Agent
    As a LiveChat Business User
    In order to find conversations with a particular agent
    I want to use the Agent filter

    @e2e-test

    Scenario: Filter by Agent
        Given I’m on the Livechat Archives page
        When I click on Add filter button
        And I pick Agent option
        And I select "Agent 007"
        Then I should see chat with "Agent 007" in chats list
