Feature: Search
    As a LiveChat Business User
    In order to find a particular conversation by key words
    I want to use the search bar
    
    @e2e-test

    Scenario: Search for chat with a client
        Given I’m on the Livechat Archives page
        When I type "Client no2" in Archives Search Bar
        Then I should see chat with "Client no2" in chats list

    Scenario: Search for chat with an agent

        Given I’m on the Livechat Archives page
        When I type "Agent 007" in Archives Search Bar
        Then I should see chat with "Agent 007" in chats list

    Scenario: Search for chat with a keyword

        Given I’m on the Livechat Archives page
        When I type "Facebook" in Archives Search Bar
        Then I should see chat with "Facebook" in chats list
